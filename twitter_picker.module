<?php

/**
* Todo - complete access callback call in hook_menu, create calback function and form and submit handler to save stuff to db
*/
/**
 * Implements hook_permission
 */
function twitter_picker_permission() {
  return array(
    'administer promoted tweets' => array(
      'title' => t('Administer promoted tweets'),
    ),
  );
}
 
/**
 * Implemens hook_menu
 */
function twitter_picker_menu() {
  $items['admin/content/promoted-tweets'] = array(
    'title' => 'Promoted Tweets',
    'description' => 'Promote individual tweets',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('twitter_picker_promoted_tweets_form'),
    'access arguments' => array('administer promoted tweets'),
  );
  return $items;
}
 
/**
 * Page callback
 * Show a paginated table of tweets, with a promote / unpromote tweet for each
 */
function twitter_picker_promoted_tweets_form() {
  
  // Get tweets from database
  $result = db_query("SELECT t.twitter_id, t.created_time, t.text, p.promoted FROM {twitter} t LEFT JOIN {twitter_picker} p ON p.twitter_id = t.twitter_id ORDER BY t.created_time DESC");
  
  // Loop through tweets and create table rows and form elements
  $options = array();
  $form = array();
  $options_checked = array();
  foreach ($result as $tweet) {
    $row = array(
      format_date($tweet->created_time),
      $tweet->text,
    );
    $options[$tweet->twitter_id] = $row;
    if ($tweet->promoted) {
      $options_checked[$tweet->twitter_id] = TRUE;
    }
  }
  
  // Create the table
  $header = array(t('Time'), t('Text'));
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#js_select' => FALSE,
    '#default_value' => $options_checked,
    '#empty' => t('No tweets found'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

function twitter_picker_promoted_tweets_form_submit($form, &$form_state) {
  // Loop through submitted values and update or insert as appropriate
  $fields = array();
  foreach($form_state['values']['table'] as $twitter_id => $promoted) {
  
    // Clear result on each loop
    unset($result);
    
    // Prepare fields
    $promoted_val = ($promoted == 0) ? 0 : 1;
    
    // Find out whether this tweet is in the db already
    $result = db_query("SELECT COUNT(*) count FROM {twitter_picker} WHERE twitter_id = :twitter_id", array(':twitter_id' => $twitter_id));
    $rows = $result->fetchAll();
    if ($rows[0]->count > 0) {
      $result = db_query("UPDATE {twitter_picker} SET promoted = :promoted WHERE twitter_id = :twitter_id", array(':twitter_id' => $twitter_id, ':promoted' => $promoted_val));
    }
    else {
      // Insert it into the database
      $result = db_query("INSERT INTO {twitter_picker} (twitter_id, promoted) VALUES (:twitter_id, :promoted)", array(':twitter_id' => $twitter_id, ':promoted' => $promoted_val));
    }
  }
}

/**
 * Implements hook_views_api()
 */
function twitter_picker_views_api() {
  return array(
    'api' => 3,
  );
}
 
/**
 * Implements hook_views_data()
 * Exposes our playcount table to views
 */
function twitter_picker_views_data() {
  // Basic table information.
  $data['twitter_picker'] = array(
    'table' => array(
      'group' => t('Twitter'),
      'join' => array(
        'twitter' => array(
          'left_field' => 'twitter_id',
          'field' => 'twitter_id',
        ),
      ),
    )
  );
 
  // Our fields
  $data['twitter_picker']['promoted'] = array(
    'title' => t('Promoted'),
    'help' => t('Whether the tweet is promoted or not.'),
  );
 
  // Adds our field in the "Fields" section of Views
  $data['twitter_picker']['promoted']['field'] = array(
    'handler' => 'views_handler_field_boolean',
    'click sortable' => TRUE,
  );
 
  // Adds our field in the "Filters" section of Views
  $data['twitter_picker']['promoted']['filter'] = array(
    'handler' => 'views_handler_filter_boolean_operator',
  );
 
  // Adds our field in the "Sort" section of Views
  $data['twitter_picker']['promoted']['sort'] = array(
    'handler' => 'views_handler_field_boolean',
  );
 
  return $data;
}